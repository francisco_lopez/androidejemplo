package cl.fjlopez.androidejemplo2;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import domain.AdministradorVO;
import domain.LoginVO;

import android.util.Log;

public class Conexion {

	/**
	 * 
	 * @param AdministradorVO
	 * @param passwd
	 * @return
	 */
	public static String autentificacion(AdministradorVO AdministradorVO) {
		String responseString = "";
		String method = "autentificacion";
		String TAG = method;
		Log.i("intentando enviar datos al ws usando: ", method);

		try {
			String SOAP_ACTION = Constantes.NAMESPACE + "/" + method;
			SoapObject request = new SoapObject(Constantes.NAMESPACE, method);

			PropertyInfo pi = new PropertyInfo();
			pi.setName("oAdministradorVO");
			pi.setValue(AdministradorVO);
			pi.setType(AdministradorVO.getClass());
			request.addProperty(pi);

			SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);

			soapEnvelope.dotNet = false;

			soapEnvelope.setOutputSoapObject(request);
			soapEnvelope.addMapping("http://domain/xsd", "AdministradorVO", new AdministradorVO().getClass());
			HttpTransportSE aht = new HttpTransportSE(Constantes.URL);
			aht.debug = true;

			aht.call(SOAP_ACTION, soapEnvelope);

			Log.i(TAG, "dump Request: " + aht.requestDump);
			Log.i(TAG, "dump Response: " + aht.responseDump);


			SoapObject resultsRequestSOAP = (SoapObject) soapEnvelope.bodyIn;
			SoapObject returnObj = (SoapObject) resultsRequestSOAP.getProperty("return");

			SoapPrimitive fechaFin = (SoapPrimitive) returnObj.getProperty("fechaFin");
			SoapPrimitive fechaLogeo = (SoapPrimitive) returnObj.getProperty("fechaLogeo");
			SoapPrimitive token = (SoapPrimitive) returnObj.getProperty("token");

			LoginVO loginOutput = new LoginVO();
			loginOutput.setFechaFin((String) fechaFin.toString());
			loginOutput.setFechaLogeo((String) fechaLogeo.toString());
			loginOutput.setToken((String) token.toString());

			domain.AdministradorVO administradorVOtmp = (AdministradorVO) returnObj.getProperty("administradorVO");
			loginOutput.setAdministradorVO(administradorVOtmp);

			administradorVOtmp = null;
			responseString = loginOutput.getToken();

		} catch (Exception e) {
			e.printStackTrace();
			// en caso de no tener acceso al web service
			responseString = "";
			Log.e(Constantes.class.getSimpleName(), "Hubo un error desconocido");
		}

		Log.i(Constantes.class.getSimpleName(), responseString);

		return responseString;

	}
}