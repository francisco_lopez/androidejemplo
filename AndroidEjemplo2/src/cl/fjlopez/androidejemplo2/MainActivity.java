package cl.fjlopez.androidejemplo2;

import domain.AdministradorVO;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	
	private EditText usuario;
	private EditText clave;
	private Button enviar;
	
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        usuario = (EditText) findViewById(R.id.editText_username);
        clave = (EditText) findViewById(R.id.editText_clave);

        enviar = (Button) findViewById(R.id.button_enviar);
        
        clave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clave.setText("");
				
			}
		});
        
        
        enviar.setOnClickListener(new OnClickListener() {
        	
        	

        
			
			@Override
			public void onClick(View arg0) {
				new doLoginTheard().execute(usuario.getText().toString(), clave.getText().toString());
			}
		});
    }
    
    
    
    



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    
    
    /**
	 * Thread que obitiene el token de autentificacion
	 * 
	 * @author Francisco
	 * 
	 */
	class doLoginTheard extends AsyncTask<String, Void, String> {
		private static final String TAG = "doLoginTheard";
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(MainActivity.this, "Conectando", "Verificando usuario y contraseņa.",
					true, true);
		}

		@Override
		protected String doInBackground(String... params) {
			// String resultadoJSON = new String();
			String user = params[0];
			String passwd = params[1];
			String result="";
			
			try {
				if (Constantes.token_auth == null || "".equals(Constantes.token_auth)) {
					result = Conexion.autentificacion(new AdministradorVO(user, passwd));
					Log.i(TAG, result);
				}
			} catch (Exception e) {
				Log.e(TAG, e.getCause().toString());
			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			

			if ("".equals(result)) {
				Toast.makeText(MainActivity.this, "Existe problemas de conectividad con el servidor",Toast.LENGTH_SHORT).show();
			} else {
				Constantes.token_auth = result;
				Toast.makeText(getApplicationContext(),"el token es : "+result , Toast.LENGTH_LONG).show();
				
			} 
		}

	}
    
}
