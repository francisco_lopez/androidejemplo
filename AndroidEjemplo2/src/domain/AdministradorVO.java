package domain;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class AdministradorVO  implements KvmSerializable{
	private String user;
	private String password;
	
	
	//constructor
	public AdministradorVO(String user, String password) {
		super();
		this.user = user;
		this.password = password;
	}
	//constructor
	public AdministradorVO() {
		super();
	}
	
	
	// getters y setters
	
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	// implementaciones de KvmSerializable
	
	@Override
	public Object getProperty(int arg0) {
		switch (arg0) {
	
		case 0:
			return this.user;
		case 1:
			return this.password;
		}
	
		return null;
	}
	@Override
	public int getPropertyCount() {
		return 2;
	}

	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo info) {
		switch (arg0) {
		case 0:
			info.type = PropertyInfo.STRING_CLASS;
			info.name = "user";
			break;
		case 1:
			info.type = PropertyInfo.STRING_CLASS;
			info.name = "password";
			break;

		default:
			break;
		}

	}

	@Override
	public void setProperty(int index, Object value) {
		switch (index) {
		case 0:
			user = value.toString();
			break;
		case 1:
			password = value.toString();
			break;
		default:
			break;
		}

	}

}
