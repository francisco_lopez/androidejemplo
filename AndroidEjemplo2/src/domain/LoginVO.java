package domain;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

public class LoginVO{
	private String fechaLogeo;
	private AdministradorVO administradorVO;
	private String token;
	private String fechaFin;
	
	/**
	 * Constructor
	 */
	public LoginVO() {
		super();
	}

	/**
	 * Constructor
	 * @param fechaLogeo
	 * @param administradorVO
	 * @param token
	 * @param fechaFin
	 */
	public LoginVO(String fechaLogeo, AdministradorVO administradorVO,
			String token, String fechaFin) {
		super();
		this.fechaLogeo = fechaLogeo;
		this.administradorVO = administradorVO;
		this.token = token;
		this.fechaFin = fechaFin;
	}


	
	/*
	 * Getters y Setters
	 * **/
	
	
	/**
	 * @return the fechaLogeo
	 */
	public String getFechaLogeo() {
		return fechaLogeo;
	}

	/**
	 * @param fechaLogeo the fechaLogeo to set
	 */
	public void setFechaLogeo(String fechaLogeo) {
		this.fechaLogeo = fechaLogeo;
	}

	/**
	 * @return the administradorVO
	 */
	public AdministradorVO getAdministradorVO() {
		return administradorVO;
	}

	/**
	 * @param administradorVO the administradorVO to set
	 */
	public void setAdministradorVO(AdministradorVO administradorVO) {
		this.administradorVO = administradorVO;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the fechaFin
	 */
	public String getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NUUU";
	}
/*
	@Override
	public Object getProperty(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setProperty(int arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
*/
}
