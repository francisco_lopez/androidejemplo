package cl.fjlopez.util;


public class MD5 {
	/**
	 * metodo que convierte una cadena en una cadena md5
	 * @param String entrada
	 * @return
	 */
	public static String convertirMd5 (String entrada){
		return org.apache.commons.codec.digest.DigestUtils.md5Hex(entrada);
	}
}
