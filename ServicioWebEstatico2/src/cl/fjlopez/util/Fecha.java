package cl.fjlopez.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Fecha {
	private Date fecha;

	/**
	 * Constructor con parametro
	 * @param fecha
	 */
	public Fecha(Date fecha) {
		super();
		this.fecha = fecha;
	}

	/**
	 * Constructor Vacio
	 */
	public Fecha() {
		super();
		this.fecha = new Date();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(this.fecha);
	}
	


}
