package cl.pds.ayudantia.negocio;

import java.util.Date;

import domain.LoginVO;

public class Negocio {
	/**
	 * 
	 * @param oAdministradorVO
	 * @return
	 */
	public LoginVO autentificacion(domain.AdministradorVO oAdministradorVO){
		System.out.println("entrada");		
		Date fecha = new Date();
		Date fechaMas10Minutos = new Date(fecha.getTime() +(10 * 60000) ); // 60000 = 1 minuto en milisegundos
		
		LoginVO login = new LoginVO();
		login.setAdministradorVO(oAdministradorVO);
		login.setFechaLogeo(new cl.fjlopez.util.Fecha(fecha).toString());
		login.setFechaFin(new cl.fjlopez.util.Fecha(fechaMas10Minutos).toString());
		login.setToken(cl.fjlopez.util.MD5.convertirMd5(oAdministradorVO.getUser()+login.getFechaLogeo())); // hash obtenido a partir de la fecha de logeo y usuario genera id unico para cada logeo
		
		fecha = null;
		fechaMas10Minutos = null;
		
		System.out.println(login.toString());
		return login;
	}
}
