package domain;

import com.google.gson.Gson;

public class LoginVO {
	private String fechaLogeo;
	private AdministradorVO administradorVO;
	private String token;
	private String fechaFin;
	
	/**
	 * Constructor
	 */
	public LoginVO() {
		super();
	}

	/**
	 * Constructor
	 * @param fechaLogeo
	 * @param administradorVO
	 * @param token
	 * @param fechaFin
	 */
	public LoginVO(String fechaLogeo, AdministradorVO administradorVO,
			String token, String fechaFin) {
		super();
		this.fechaLogeo = fechaLogeo;
		this.administradorVO = administradorVO;
		this.token = token;
		this.fechaFin = fechaFin;
	}


	
	/*
	 * Getters y Setters
	 * **/
	
	
	/**
	 * @return the fechaLogeo
	 */
	public String getFechaLogeo() {
		return fechaLogeo;
	}

	/**
	 * @param fechaLogeo the fechaLogeo to set
	 */
	public void setFechaLogeo(String fechaLogeo) {
		this.fechaLogeo = fechaLogeo;
	}

	/**
	 * @return the administradorVO
	 */
	public AdministradorVO getAdministradorVO() {
		return administradorVO;
	}

	/**
	 * @param administradorVO the administradorVO to set
	 */
	public void setAdministradorVO(AdministradorVO administradorVO) {
		this.administradorVO = administradorVO;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the fechaFin
	 */
	public String getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

}
