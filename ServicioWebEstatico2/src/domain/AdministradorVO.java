package domain;

import com.google.gson.Gson;

public class AdministradorVO {
private String user;
private String password;


//constructor
public AdministradorVO(String user, String password) {
	super();
	this.user = user;
	this.password = password;
}

//constructor
public AdministradorVO(String json) {
	super();
	AdministradorVO object = new Gson().fromJson(json, AdministradorVO.class);
	this.password = object.password;
	this.user = object.user;
	object = null;
}
//constructor
public AdministradorVO() {
	super();
}


// getters y setters

/**
 * @return the user
 */
public String getUser() {
	return user;
}
/**
 * @param user the user to set
 */
public void setUser(String user) {
	this.user = user;
}
/**
 * @return the password
 */
public String getPassword() {
	return password;
}
/**
 * @param password the password to set
 */
public void setPassword(String password) {
	this.password = password;
}
/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override
public String toString() {
	return new Gson().toJson(this);
}

}
