import java.util.Date;

import cl.fjlopez.util.Encriptacion;
import domain.AdministradorVO;
import domain.LoginVO;


public class Main {


	public static void main(String[] args) {
		
		Date fecha = new Date();
		Date fechaMas10Minutos = new Date(fecha.getTime() +(10 * 60000) ); // 60000 = 1 minuto en milisegundos
		
		AdministradorVO usuario = new AdministradorVO();
		usuario.setUser("user");
		usuario.setPassword("password");
		
		LoginVO login = new LoginVO();
		login.setAdministradorVO(usuario);
		login.setFechaLogeo(new cl.fjlopez.util.Fecha(fecha).toString());
		login.setFechaFin(new cl.fjlopez.util.Fecha(fechaMas10Minutos).toString());
		login.setToken(cl.fjlopez.util.MD5.convertirMd5(usuario.getUser()+login.getFechaLogeo())); // hash obtenido a partir de la fecha de logeo y usuario genera id unico para cada logeo
		try {
			System.out.println(Encriptacion.encriptar(login.getAdministradorVO().getUser()));
			
			System.out.println(Encriptacion.desencriptar("abFxwmALAyEes2753cq8fQ=="));		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		


	}

}
